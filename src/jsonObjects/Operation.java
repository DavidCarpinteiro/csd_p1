package jsonObjects;

public class Operation {

    public String op;
    public Object data;

    public Operation(String op, Object data) {
        this.op = op;
        this.data = data;
    }
}
