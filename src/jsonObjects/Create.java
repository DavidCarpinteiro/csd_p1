package jsonObjects;

public class Create {

    private String owner;
    private Double amount;

    public Create() {

    }

    public Create(String owner, Double amount) {

        if (owner == null || amount == null) {
            throw new RuntimeException("NULL values");
        }

        this.owner = owner;
        this.amount = amount;

    }

    public String getOwner() {
        return owner;
    }


    public Double getAmount() {
        return amount;
    }
}
