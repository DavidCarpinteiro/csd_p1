package jsonObjects;

public class OwnerInfo {

    private String owner;

    public OwnerInfo() {

    }

    public OwnerInfo(String owner) {
        this.owner = owner;
    }

    public String getOwner() {
        return owner;
    }
}
