package jsonObjects;

public class AmountResponse {

    private double amount;

    public AmountResponse() {

    }

    public AmountResponse(double amount) {
        this.amount = amount;
    }

    public double getAmount() {
        return amount;
    }
}
