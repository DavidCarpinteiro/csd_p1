package jsonObjects;

public class Transfer {

    private String to, from;
    private Double amount;

    public Transfer() {

    }

    public Transfer(String to, String from, Double amount) {

        if (to == null || from == null || amount == null) {
            throw new RuntimeException("NULL values");
        }

        this.to = to;
        this.from = from;
        this.amount = amount;

    }

    public String getTo() {
        return to;
    }


    public String getFrom() {
        return from;
    }


    public Double getAmount() {
        return amount;
    }

}
