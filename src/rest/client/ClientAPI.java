package rest.client;

import jsonObjects.AmountResponse;
import jsonObjects.Create;
import jsonObjects.OwnerInfo;
import jsonObjects.Transfer;
import org.glassfish.jersey.client.ClientConfig;
import rest.server.Server;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

class ClientAPI {

    private static int ID = 1;

    final private WebTarget target;
    final private String userID;

    static public class InsecureHostnameVerifier implements HostnameVerifier {
        @Override
        public boolean verify(String hostname, SSLSession session) {

            return true;
        }
    }

    ClientAPI(int port) {
        //ClientConfig config = new ClientConfig();
       // Client client = ClientBuilder.newClient(config);

        System.out.println("STARTING CLIENT ON PORT: " + port);

        Client client = ClientBuilder.newBuilder()
                .hostnameVerifier(new InsecureHostnameVerifier())
                .build();

        URI baseURI = UriBuilder.fromUri("https://localhost").port(port).build();

        target = client.target(baseURI);
        userID = "user" + ID++;
    }

    String getUserID() {
        return userID;
    }

    void create_money(double amount) {
        System.out.printf("Creating Money <%s> ($%f)\n", userID, amount);

        Response response = target
                .path("create_money")
                .request()
                .post(Entity.json(new Create(userID, amount)));

        System.out.printf("\tReply: %d\n", response.getStatus());
    }

    void transfer_money(String to, double amount) {
        System.out.printf("Transferring Money from <%s> to <%s> amount ($%f)\n", userID, to, amount);

        Response response = target
                .path("transfer_money")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(new Transfer(to, userID, amount)));

        System.out.printf("\tReply: %d\n", response.getStatus());

        if (response.getStatus() == 200) {
            System.out.printf("\tAmount Left ($%f)\n", response.readEntity(AmountResponse.class).getAmount());
        }

    }

    void check_money() {
        System.out.printf("Checking money <%s>\n", userID);

        Response response = target
                .path("check_money")
                .request(MediaType.APPLICATION_JSON)
                .put(Entity.json(new OwnerInfo(userID)));

        System.out.printf("\tReply: %d\n", response.getStatus());

        if (response.getStatus() == 200) {
            System.out.printf("\tMoney ($%f)\n", response.readEntity(AmountResponse.class).getAmount());
        }

    }


}
