package rest.server;

import bftsmart.demo.bftmap.BFTMapServer;
import bftsmart.tom.MessageContext;
import bftsmart.tom.ServiceProxy;
import bftsmart.tom.ServiceReplica;
import bftsmart.tom.core.messages.TOMMessage;
import bftsmart.tom.server.defaultservices.DefaultSingleRecoverable;
import bftsmart.tom.util.Extractor;
import jsonObjects.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.*;
import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import static javax.ws.rs.core.Response.Status.CONFLICT;
import static javax.ws.rs.core.Response.Status.NOT_ACCEPTABLE;



@Path("/")
public class ServerAPI extends DefaultSingleRecoverable {

    private Map<String, Double> db = new ConcurrentHashMap<>();

    private final ServiceProxy serviceProxy;

    public enum ACTIONS {
        CREATE,
        TRANSFER,
        CHECK
    }

    ServerAPI(int id, int bft_port) {
        System.out.println("STARTING CLIENT ON PORT: " + bft_port);
        System.err.println("\t\tLaunching BFT Server on port: " + bft_port);

/*        Thread t1 = new Thread(new Runnable() {
            public void run()
            {
                // code goes here.
            }});
        t1.start();*/

        new ServiceReplica(id, this, this);

        Extractor ext = new Extractor() {
            @Override
            public TOMMessage extractResponse(TOMMessage[] tomMessages, int i, int i1) {
                //guardar array de msg numa var local para depois poder enviar
                //chamado no invoe ordered
                //novo metodo para returnar msg para o cliente
                return null;
            }
        };//escolher



        Comparator<byte[]> cmp = new Comparator<byte[]>() {
            @Override
            public int compare(byte[] bytes, byte[] t1) {
                return 0;
            }
        };//comparar resultaos

        this.serviceProxy = new ServiceProxy(bft_port, "config", cmp, ext);

        System.err.println("\t\tBFT running...");
    }

    @POST
    @Path("create_money")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create_money(Create create) {
        System.err.printf("Creating Money: <%s> %f$\n", create.getOwner(), create.getAmount());


        //throw new WebApplicationException(CONFLICT);
        Operation op = new Operation("create", create);


        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut);) {

            objOut.writeObject("create");
            objOut.writeObject(create);

            objOut.flush();
            byteOut.flush();

            byte[] reply = serviceProxy.invokeOrdered(byteOut.toByteArray());
            if (reply.length == 0)
                return Response.status(201).build();
            try (ByteArrayInputStream byteIn = new ByteArrayInputStream(reply);

                 ObjectInput objIn = new ObjectInputStream(byteIn)) {

                Reply r = (Reply) objIn.readObject();

                System.out.println(r);

                return Response.status(200).build();
            }

        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Exception putting value into map: " + e.getMessage());
            return Response.status(409).build();
        }

    }

    @PUT
    @Path("transfer_money")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response transfer_money(Transfer transfer) {
        System.err.printf("Transferring Money: from <%s> to <%s>, amount: %f$\n", transfer.getFrom(), transfer.getTo(), transfer.getAmount());

        if (!db.containsKey(transfer.getFrom()) || !db.containsKey(transfer.getTo())) {
            System.err.print("\tUser does not exist\n");
            throw new WebApplicationException(NOT_ACCEPTABLE);
        }

        double from_wallet = db.get(transfer.getFrom()) - transfer.getAmount();
        double to_wallet = db.get(transfer.getTo()) + transfer.getAmount();

        db.put(transfer.getFrom(), from_wallet);
        db.put(transfer.getTo(), to_wallet);

        System.err.printf("\tTransfer complete: from has %f$, to has %f$\n", from_wallet, to_wallet);

        return Response.ok(new AmountResponse(from_wallet), MediaType.APPLICATION_JSON).build();
    }

    @PUT
    @Path("check_money")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response check_money(OwnerInfo ownerInfo) {
        System.err.printf("Checking Money: from %s \n", ownerInfo.getOwner());

        if (!db.containsKey(ownerInfo.getOwner())) {
            System.err.print("\tUser does not exist\n");
            throw new WebApplicationException(NOT_ACCEPTABLE);
        }

        double amount = db.get(ownerInfo.getOwner());
        System.err.printf("\tUser has %f$\n", amount);

        return Response.ok(new AmountResponse(amount), MediaType.APPLICATION_JSON).build();
    }

    private void doTransfer() {

    }

    private void doCreate(Create c) {
        if (db.containsKey(c.getOwner())) {
            System.err.print("\tUser already exists\n");
            //throw new WebApplicationException(CONFLICT);
        }

        db.put(c.getOwner(), c.getAmount());

        System.err.print("\tUser Created\n");
    }

    private void doCurrent() {

    }

    @Override
    public void installSnapshot(byte[] bytes) {
        try {

            // serialize to byte array and return
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            ObjectInput in = new ObjectInputStream(bis);
            db = (Map<String, Double>) in.readObject();
            in.close();
            bis.close();

        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BFTMapServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BFTMapServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public byte[] getSnapshot() {
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(db);
            out.flush();
            bos.flush();
            out.close();
            bos.close();
            return bos.toByteArray();
        } catch (IOException ex) {
            Logger.getLogger(BFTMapServer.class.getName()).log(Level.SEVERE, null, ex);
            return new byte[0];
        }
    }

    @Override
    public byte[] appExecuteOrdered(byte[] bytes, MessageContext messageContext) {

        //executar protocologo

        try (ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
             ObjectOutput objOut = new ObjectOutputStream(byteOut)) {

            if (bytes.length == 0)
                return null;
            try (ByteArrayInputStream byteIn = new ByteArrayInputStream(bytes);

                 ObjectInput objIn = new ObjectInputStream(byteIn)) {

                String s = (String) objIn.readObject();

                /*
                 int cmd = new DataInputStream(in).readInt();
	        switch (cmd) {
                case BFTMapRequestType.SIZE_TABLE:
                 */

                switch (s.toLowerCase()) {
                    case "create":
                        Create c = (Create) objIn.readObject();
                        doCreate(c);
                        objOut.writeObject(new Reply(2000d));
                        break;
                }
            }

            objOut.flush();
            byteOut.flush();

            return byteOut.toByteArray();
        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Exception putting value into map: " + e.getMessage());
        }

        return new byte[0];
    }

    @Override
    public byte[] appExecuteUnordered(byte[] bytes, MessageContext messageContext) {
        return new byte[0];
    }

}
