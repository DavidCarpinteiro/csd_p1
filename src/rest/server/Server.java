package rest.server;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import javax.net.ssl.SSLContext;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.security.NoSuchAlgorithmException;

public class Server  {

    public static void main(String[] args) throws NoSuchAlgorithmException {
        System.setProperty("Djavax.net.debug", "all");
        System.setProperty("https.protocols", "TLSv1,TLSv1.1,TLSv1.2");
        System.setProperty("Djavax.net.ssl.keyStore", "server.jks");
        System.setProperty("Djavax.net.ssl.keyStorePassword", "changeme");

        int id, rest_port, bft_port;

        if (args.length != 3) {

            throw new RuntimeException("Usage: [ID] [REST PORT] [BFT PORT]");

        }

        id = Integer.parseInt(args[0]);
        rest_port = Integer.parseInt(args[1]);
        bft_port = Integer.parseInt(args[2]);


        System.out.printf("Using [%d] [%d] [%d]\n", id, rest_port, bft_port);


        URI baseUri = UriBuilder.fromUri("https://0.0.0.0/").port(rest_port).build();

        ResourceConfig config = new ResourceConfig();
        config.register(new ServerAPI(id, bft_port));

        JdkHttpServerFactory.createHttpServer(baseUri, config, SSLContext.getDefault());


        System.err.println("\t\tREST Server " + id + " ready @ " + baseUri);

    }

}
